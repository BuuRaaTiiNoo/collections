package com.epam.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    //Просто лист
    private List<String> arrayList;
    //Форматироване строки
    private String modString(String s) {
        s = s.toLowerCase().replaceAll("[^A-Za-zА-Яа-я]\\s+", " ");
        return s;
    }
    //Компаратор
    class WordComparator implements Comparator<String> {
        @Override
        public int compare(String word1, String word2) {
            return String.valueOf(arrayList.stream().filter(word1::equals).count())
                    .compareTo(String.valueOf(arrayList.stream().filter(word2::equals).count()));
        }
    }

    //в порядке возрастания
    private List<String> getWordByUp(String text) {
        arrayList = new ArrayList<>(Arrays.asList(modString(text).split("\\s")));
        return arrayList
                .stream()
                .sorted(new WordComparator().reversed())
                .distinct()
                .limit(20)
                .collect(Collectors.toList());
    }
    //в порядке убывания
    private List<String> getWordByDown(String text) {
        arrayList = new ArrayList<>(Arrays.asList(modString(text).split("\\s")));
        return arrayList
                .stream()
                .sorted(new WordComparator())
                .distinct()
                .limit(20)
                .collect(Collectors.toList());
    }

    private String readUsingBufferedReader() throws IOException {
        File file = new File("text.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String text = "";
        while ((line = br.readLine()) != null) {
            text = text.concat(line);
        }
        br.close();
        fr.close();
        return text;
    }

    public static void main(String[] args) throws IOException {
        String text = new Main().readUsingBufferedReader();
        System.out.println("20 самых популярных слов: " + new Main().getWordByUp(text));
        System.out.println("20 самых редкоиспользуемых слов: " + new Main().getWordByDown(text));
    }
}
