package com.epam.tasks;

import java.io.*;
import java.util.*;

public class PopularWord {

    private String readUsingBufferedReader() throws IOException {
        File file = new File("text.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String text = "";
        while ((line = br.readLine()) != null) {
            text = text.concat(line);
        }
        br.close();
        fr.close();
        return text;
    }

    private String modString(String s) {
        s = s.toLowerCase()
                .replaceAll("[^A-Za-zА-Яа-я]\\s+", " ");
        return s;
    }

    private Map<String, Long> insertIntoMap() throws IOException {
        Map<String, Long> map = new HashMap<>();
        for (String s : modString(readUsingBufferedReader()).split("\\s")) {
            map.put(s, map.containsKey(s) ? map.get(s) + 1 : 1);
        }
        return map;
    }

    private List<String> getUniqueWords() throws IOException {
        List<String> entryList = new ArrayList<>();
        for (Map.Entry entry : insertIntoMap().entrySet()) {
            String key = (String) entry.getKey();
            Long value = (Long) entry.getValue();
            if (value == 1) {
                entryList.add(key);
            }
        }
        return entryList;
    }

    private List<String> getSortedList(List<String> list) {
        list.sort(String::compareTo);
        return list;
    }

    public static void main(String[] args) throws IOException {
        PopularWord popularWord = new PopularWord();
        String str = popularWord.modString(popularWord.readUsingBufferedReader());
        System.out.println("Text as string line: \n" + str);
        System.out.println("Sorted words in text by name: \n" + popularWord.getSortedList(Arrays.asList(str.split("\\s"))));
        System.out.println("Words and its repeat in text: \n" + popularWord.insertIntoMap());
        System.out.println("Unique words in text: \n" + popularWord.getUniqueWords());
    }
}

